#About Kali
#
#
FROM kalilinux/kali

MAINTAINER Dubu Qingfeng <1135326346@qq.com>

RUN rm /etc/apt/sources.list

ADD sources.list /etc/apt/sources.list

ADD startup.sh /startup.sh

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -y update && apt-get -y dist-upgrade && apt-get clean

# install desktop
RUN apt-get -y install kali-defaults kali-root-login desktop-base xfce4 xfce4-places-plugin xfce4-goodies

RUN apt-get install -y git x11vnc wget python python-numpy unzip xvfb openbox geany menu && \
    cd /root && git clone https://github.com/kanaka/noVNC.git && \
    cd noVNC/utils && git clone https://github.com/kanaka/websockify websockify && \
    cd /root && \
    chmod 0755 /startup.sh && \
    rm -rf /var/lib/apt/lists/*

CMD /startup.sh
EXPOSE 6080
